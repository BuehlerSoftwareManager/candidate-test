# Buehler candidate-test

Hi, 

Here is the small MVVM example. Most of our projects are built in MVVM architecture.

For this test, a simple demo program allows a user to enter patient details which we will display in a list.

We have Id, Name and Mobile number fields already. You need to add Date of Birth field and also
when you click on add button mobile number and Date of birth should be displayed on the list . 

Ref: Add button is enabled only when you enter all fields and go back to Id field.

You will also find a build error bug when you run the project for the first time. This error should be debugged and fixed.

you can run this project in visual studio community edition.

Please don't send this project by email. Upload it to onedrive, Dropbox etc., or create your own bitbucket account and send us the link we 
will dowload your project.

Good Luck.


#Another test

Buehler Application Software Engineer Technical Proficiency Test 
This test is designed to demonstrate your understanding of how a WPF (Windows platform) application is constructed including the user interface, database, framework, C# language, and dependencies.
You need to build a small windows application(WPF) with the below requirements,
-	Create a login UI with fields username/Email ID and password.
-	Create registration UI with First Name, Last Name, Email ID, Password, and Confirm Password.
-	Track all log-in’s in the database with a time stamp.
-	Once logged in, have a button to generate a list of all the log-in that shows the Name and timestamp. 
Required software:
•	Visual Studio Community edition - https://visualstudio.microsoft.com/downloads/
•	SQL server management studio - https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver15
Getting Started:
Getting Started
1.	Create a new project Go to File – New – Project and select WPF application in Visual Studio and start working on your project.
2.	Make sure to follow the MVVM (Model, View, ViewModel)Pattern.
3.	Make Sure you also do field validations for Login and Registration.
4.	Feel free to modify the User Interface


  
